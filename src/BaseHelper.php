<?php

namespace Tetrapak07\Emailer;

use Phalcon\Di;

/**
 * Description of BaseHelper
 *
 */
abstract class BaseHelper
{
    protected $di;
    
    protected function setDi()
    {
        $this->di = Di::getDefault();
    }
   
}
