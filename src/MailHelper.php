<?php

namespace Tetrapak07\Emailer;

use Supermodule\ControllerBase as SupermoduleBase;

/**
 * Description of MailHelper
 *
 */
class MailHelper extends BaseHelper
{
    
    /**
     * @var Phalcon\Mvc\View
     */
    private $view;
    /**
     * @var Phalcon\Config
     */
    private $config;
    /**
     * @var MailHelper
     */
    static private $instance;
    
    static public $dataReturn;

    /**
     * Initialize helper
     */
    private function __construct()
    {
        self::$dataReturn = true; 
        if (!SupermoduleBase::checkAndConnectModule('emailer')) {
             self::$dataReturn = false;
           //header("Location: /index/show404");
           //exit;
        }   
        $this->setDi();
        $this->view = $this->di->get('view');
        $this->config = $this->di->get('config')->modules->emailer;
    }

    /**
     * Get current instanse of this class
     * @return MailHelper
     */
    static public function getInsctance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self;
        }
        if (!self::$dataReturn) {
            return false;
        }
        return self::$instance;
    }

    /**
     * Rendering email template
     * @param array $params Params for view
     * @param string $type Name of template
     * @return string
     */
    private function render($params = [], $type, $data = false)
    {
        if ($data) {
           foreach ($data as $key => $val) { 
             $this->view->$key = $val;
           }
        }
        $html = $this->view->getPartial($type, $params);
       // echo $html;exit;
        return $html;
    }
    
    /**
     * Rendering email template public
     * @param array $params Params for view
     * @param string $type Name of template
     * @return string
     */
    static public function renderOut($params = [], $type, $data = false)
    {
         $instance = self::getInsctance();
         if (! $instance) {
             return false;
         }
         return $instance->render($params, $type, $data);
    }

    /**
     * Sending email
     * @param string $type Type of letter
     * @param string $to Email of recipient
     * @param string|int $locale Locale of recipient
     * @param array $params Params for view
     * @return boolean
     */
    static public function send($type, $to, $locale = 1, $timeSend = '', $params = [])
    {
        $instance = self::getInsctance();
        if (! $instance) {
             return false;
         }
        $templateData = $instance->getTempateData($type, $locale);
        $html = $instance->render($params, $type);
        $response = $instance->sendMail($to, $templateData, $html, $timeSend);
        return $instance->parseResponse($response);
    }

    /**
     * Get data from DB
     * @param string $type Type of letter
     * @param string|int $locale Locale of recipient
     * @return EmailTemplate
     * @throws Exception Invalid params
     */
    private function getTempateData($type, $locale)
    {
        $this->getLocale($locale);
        $templateData = EmailTemplates::findFirst([
                    'conditions' => 'type = :type: and localeId = :locale:',
                    'bind' => array(
                        'type' => $type,
                        'locale' => $locale
                    )
        ]);

        if ($templateData) {
            return $templateData;
        }

        throw new Exception('Template data of type = ' . $type . ' and locale = ' . $locale . ' is not found');
    }

    /**
     * Convert locale from int to string
     * @param int|string $locale Recipient's locale
     * @throws Exception Invalid locale ID
     */
    private function getLocale(&$locale)
    {
        if (is_int($locale)) {
            $localeData = Locale::findFirst($locale);
            if ($localeData) {
                $locale = $localeData->id;
            } else {
                throw new Exception('Locale ID ' . $locale . ' is not defined');
            }
        }
    }

    /**
     * Send email by Mailchimp
     * @param string $to Recipient's email
     * @param EmailTemplate $templateData Data from DB
     * @param string $html Rendered template
     * @return array Answer
     */
    private function sendMail($to, $subject, $html, $companyName)
    {
        $mandrill = new \Mandrill($this->config->mandrillApiKey);
        $message = new \StdClass();
        $message->html = $html;
        $message->text = $html;
        $message->subject = $subject;
        $message->from_email = $this->config->sender;
        $message->from_name = $companyName? : $this->config->senderName;
        $message->to = array(array('email' => $to));
        $message->track_opens = true;
        return $mandrill->messages->send($message);
    }
    
    static public function sendMailOut($to, $subject, $html, $companyName)
    {
         $instance = self::getInsctance();
         if (! $instance) {
             return false;
         }
         return $instance->sendMail($to, $subject, $html, $companyName);
    }

    /**
     * Parsing Mailchimp answer
     * @param array $response Mailchimp answer
     * @return boolean
     */
    private function parseResponse($response)
    {
        return isset($response[0]['status']) && $response[0]['status'] == self::STATUS_SENT;
    }
    
     /**
     * Send API request by Mailchimp
     * @param string $methodName API method name
     * @param string $methodType Method type (get, post, delete etc.)
     * @param array $data Additional params
     * @return array
     */
    private function getMailChimpMethods($methodName = '/', $methodType = 'get', $data = []) 
    {
        $MailChimp = new \MailChimp($this->config->mailChimpApiKey);
        return $MailChimp->$methodType($methodName, $data);
    }
    
     /**
     * Implements the public method for Mailchimp API
     * @param string $method API method name
     * @param string $methodType Method type
     * @param array $data Additional params
     * @return array
     */
    static public function mailChimpMethod($method, $methodType, $data, $templType = '', $params = [], $dataTempl = false)
    {
        $instance = self::getInsctance();
        if (! $instance) {
             return false;
         }
        if (isset($data['html'])) {
          $data['html'] = $instance->render($params, $templType, $dataTempl); 
        }
        $retMethod = $instance->getMailChimpMethods($method, $methodType, $data);
        return $retMethod;
    }
   
}
