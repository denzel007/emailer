<?php

namespace App\Helpers;

use  App\Controllers\ControllerBase;

/**
 * Description of MailHelper
 *
 */
class MailHelper extends \Tetrapak07\Emailer\MailHelper
{
    
    private function __construct()
    {
        parent::__construct();
        
        $base = new ControllerBase();
        
        $base->onConstruct();
    }
}
